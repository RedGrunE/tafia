﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace GrmmarGenerator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var insertText = "→";
            var selectionIndex = textBox1.SelectionStart;
            textBox1.Text = textBox1.Text.Insert(selectionIndex, insertText);
            textBox1.SelectionStart = selectionIndex + insertText.Length;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var insertText = "ε";
            var selectionIndex = textBox1.SelectionStart;
            textBox1.Text = textBox1.Text.Insert(selectionIndex, insertText);
            textBox1.SelectionStart = selectionIndex + insertText.Length;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Grammar gr = new Grammar();
            listBox1.Items.Clear();
            foreach (string s in textBox1.Lines.Where(x => x != ""))
            {
                gr.AddRule(s);
            }
            var hs = new HashSet<string>();
            gr.Generate(textBox1.Text[0].ToString(), 0, hs, Convert.ToInt32(numericUpDown1.Value), Convert.ToInt32(numericUpDown2.Value));
            hs.ToList().ForEach(x => listBox1.Items.Add(x));
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Grammar gr = new Grammar();
            foreach (string s in textBox1.Lines)
            {
                gr.AddRule(s);
            }
            MessageBox.Show(gr.getType().ToString() + " тип");
        }
    }
}
