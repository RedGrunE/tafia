﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrmmarGenerator
{
    class Rule
    {
        public string NoTerminal;
        public string Action;

        public Rule(string NoTerminal, string Action)
        {
            this.Action = Action;
            this.NoTerminal = NoTerminal;
        }
    }
}
