﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace GrmmarGenerator
{
    class Grammar
    {
        static readonly Random rnd = new Random();
        public List<Rule> Rules { get; set; }
        public List<char> alphabet { get; set; }

        public Grammar()
        {
            string Input = "a,b,c,d,e,f,g,h,i,g,k,l,x";     //??
            Rules = new List<Rule>();
            var a = Input.Split(',').ToList();              //??
            var chars = a.SelectMany(s => s.ToCharArray()); //??
            alphabet = chars.Distinct().ToList();           //??
            alphabet.Add('ε');
        }

        public void AddRule(string rule)
        {
            var s = rule.Split(new[] { '→' }, StringSplitOptions.RemoveEmptyEntries);
            var NoTerminal = s[0];
            foreach (string Action in s[1].Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries))
            {
                Rules.Add(new Rule(NoTerminal, Action));
            }
        }

        public List<char> GetNoterminals(string str)            //????
        {
            var result = new List<char>();
            foreach (char c in str)
            {
                if (!alphabet.Contains(c)) result.Add(c);
            }
            return result;
        }

        public int getType()
        {
            var result = 0;
            if (Rules.All(x => x.NoTerminal.Length <= x.Action.Length)) result = 1; else return result;
            if (Rules.All(x => x.NoTerminal.Length == 1)) result = 2; else return result;
            if (Rules.All(x => !GetNoterminals(x.Action).Any() ||
                (GetNoterminals(x.Action).Count() == 1 &&
                (GetNoterminals(x.Action).Contains(x.Action[0])) ||  
                GetNoterminals(x.Action).Contains(x.Action[x.Action.Length - 1])))
            ) result = 3;
            return result;
        }

        public void Generate(string str, int Depth, HashSet<string> hs, int MinLength, int MaxLength)
        {
            if (Depth >= 10) return;
            if (!HasNoTerminal(str) && str.Length >= MinLength && str.Length <= MaxLength)
            {
                hs.Add(str.Replace("ε", ""));
            }
            var rules = Rules.Where(x => str.Contains(x.NoTerminal)).ToList();
            foreach (var x in rules)
            {
                var regex = new Regex(x.NoTerminal);
                Generate(regex.Replace(str, x.Action, 1), Depth + 1, hs, MinLength, MaxLength);
            }
            return;
        }

        bool HasNoTerminal(string s)
        {
            return s.Any(x => x >= 'A' && x <= 'Z');
        }
    }
}
