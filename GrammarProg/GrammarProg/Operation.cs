﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrammarProg
{
    class Operation
    {
        public string NoTerminal;
        public string Action;
        public string Index;
        public bool Producing;
        public bool Reachability;
        public Operation(string NoTerminal, string Action)
        {
            this.Action = Action;
            this.NoTerminal = NoTerminal;
            Producing = false;
            Reachability = false;
        }
        public Operation(string NoTerminal, string Action, string Index)
        {
            this.Action = Action;
            this.NoTerminal = NoTerminal;
            this.Index = Index;
            Producing = false;
            Reachability = false;
        }
    }
}
