﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace GrammarProg
{
    class Grammar
    {
        public List<Operation> Operations { get; set; }

        public Grammar()
        {
            Operations = new List<Operation>();
        }

        public void AddRule(List<string> Input)
        {
            foreach (var i in Input)
            {
                var SplitString = i.Split(new[] { '→' }, StringSplitOptions.RemoveEmptyEntries);
                var NoTerminal = SplitString[0];
                foreach (string Action in SplitString[1].Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    Operations.Add(new Operation(NoTerminal, Action));
                }
            }
        }

        public void Generate(string InputString, int Deep, HashSet<string> ManyOfChains, int MinLength, int MaxLength, bool ReplaceEpsilon)
        {
            if (Deep >= 10) return;
            if (!(InputString.Any(x => x >= 'A' && x <= 'Z')) && InputString.Length >= MinLength && InputString.Length <= MaxLength)
                ManyOfChains.Add(ReplaceEpsilon ? InputString : InputString.Replace("ε", ""));
            foreach (var x in Operations.Where(x => InputString.Contains(x.NoTerminal)).ToList())
            {
                var Reg = new Regex(x.NoTerminal);
                Generate(Reg.Replace(InputString, x.Action, 1), Deep + 1, ManyOfChains, MinLength, MaxLength, ReplaceEpsilon);
            }
        }

        public int GetTypeOfGrammar()
        {
            int TypeOfGrammar = 0;
            if (Operations.All(x => x.NoTerminal.Length <= x.Action.Length)) TypeOfGrammar = 1; else return TypeOfGrammar;
            if (Operations.All(x => x.NoTerminal.Length == 1)) TypeOfGrammar = 2; else return TypeOfGrammar;
            if (Operations.All(x => !x.Action.Where(y => char.IsUpper(y)).ToList().Any() ||
                (x.Action.Where(y => char.IsUpper(y)).ToList().Count() == 1 &&
                (x.Action.Where(y => char.IsUpper(y)).ToList().Contains(x.Action[0])) ||
                x.Action.Where(y => char.IsUpper(y)).ToList().Contains(x.Action[x.Action.Length - 1])))
            ) TypeOfGrammar = 3;
            return TypeOfGrammar;
        }

        public void DeleteOfNonProducing(string Input, Grammar MyGrammar)
        {
            var Producing = new List<char>();
            bool exit = false;
            while (!exit)
            {
                exit = true;
                foreach (var item in MyGrammar.Operations)
                {
                    bool ThisProd = true;
                    foreach (var Elements in item.Action)
                        if ((Elements >= 'A' && Elements <= 'Z') && !(Producing.Contains(Elements)))
                            ThisProd = false;
                    if (ThisProd && (!Producing.Contains(item.NoTerminal[0]) || !item.Producing))
                    {
                        item.Producing = true;
                        Producing.Add(item.NoTerminal[0]);
                        exit = false;
                    }
                }
            }
            MyGrammar.Rules.RemoveAll(x => !x.Producing);
            void ReachabilityCheck(string str, int deep)
            {
                if (deep >= 20) return;
                var l = MyGrammar.Rules.Where(x => x.NoTerminal == Convert.ToString(str[0]));
                foreach (var item in l)
                {
                    item.Reachability = true;
                    var x = item.Action.Where(char.IsUpper).ToList();
                    foreach (var i in x)
                        ReachabilityCheck(Convert.ToString(i), ++deep);
                }
            }
            ReachabilityCheck(Input, 0);
            MyGrammar.Rules.RemoveAll(x => !x.Reachability);
            //var l = MyGrammar.Rules.Select(x => new { Item = x, Cnt = x.Action.Distinct().Where(char.IsUpper).ToList() }).OrderBy(x => x.Cnt.Count).ToList();
            //var CheckedRooles = new Dictionary<string, bool>();
            //foreach (var item in l)
            //{
            //    item.Item.Producing = item.Cnt.Count == 0 || item.Cnt.All(x => CheckedRooles.Keys.Any(v => v.Contains(x)));
            //    if (!item.Item.Producing) continue;
            //    CheckedRooles[item.Item.NoTerminal] = item.Item.Producing;
            //}
            //MyGrammar.Rules.RemoveAll(x => !x.Producing);
            //MyGrammar.Rules.ForEach(item => item.Reachability = item.NoTerminal[0] == Input[0]);
        }



    }
}
