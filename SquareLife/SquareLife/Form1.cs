﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

namespace SquareLife
{
    public partial class Form1 : Form
    {
        const int size = 50;        
        Cell[,] cells = new Cell[size, size];
        Bitmap DrawArea;
        bool Time = false;

        public Form1()
        {
            InitializeComponent();
            for (int x = 0; x < size; x++)
                for (int y = 0; y < size; y++)
                    cells[x, y] = new Cell();
            DrawArea = new Bitmap(pictureBox1.Size.Width, pictureBox1.Size.Height);
            var g = Graphics.FromImage(DrawArea);
            var dx = (pictureBox1.Size.Width / size);
            var dy = (pictureBox1.Size.Height / size);
            for (int x = 0; x < size; x++)
                for (int y = 0; y < size; y++)
                {
                    g.FillRectangle(new SolidBrush(Color.FromArgb(255, 0, 0, 0)), x * dx, y * dy, dx, dy);
                    g.DrawRectangle(Pens.Gray, x * dx, y * dy, dx, dy);
                }
            pictureBox1.Image = DrawArea;
            g.Dispose();


            using (FileStream streamR = new FileStream("Save.dat", FileMode.OpenOrCreate))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                Dictionary<string, List<Point>> MasSave = new Dictionary<string, List<Point>>();
                try
                {
                    MasSave = formatter.Deserialize(streamR) as Dictionary<string, List<Point>>;
                    streamR.Close();
                }
                catch (Exception ex)
                {
                    //MessageBox.Show(ex.Message);
                }
                listBox1.Items.Clear();
                foreach (var item in MasSave)
                {
                    listBox1.Items.Add(item.Key.ToString());
                }
            }
        }

        private new void Show()
        {
            var g = Graphics.FromImage(DrawArea);
            var dx = (pictureBox1.Size.Width / size);
            var dy = (pictureBox1.Size.Height / size);
            for (int x = 0; x < size; x++)
            {
                for (int y = 0; y < size; y++)
                {
                    if (cells[x, y].Status)
                    {
                        g.FillRectangle(new SolidBrush(Color.FromArgb(255, 255, 255, 255)), x * dx, y * dy, dx, dy);
                        g.DrawRectangle(Pens.Gray, x * dx, y * dy, dx, dy);
                    }
                    else
                    {
                        g.FillRectangle(new SolidBrush(Color.FromArgb(255, 0, 0, 0)), x * dx, y * dy, dx, dy);
                        g.DrawRectangle(Pens.Gray, x * dx, y * dy, dx, dy);
                    }
                }
            }
            pictureBox1.Image = DrawArea;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            var a = e.X;
            var x = e.X / (DrawArea.Size.Width / size);
            var y = e.Y / (DrawArea.Size.Width / size);
            cells[x, y].Status = !cells[x, y].Status;
            var p = cells[x, y].Status;
            Show();
        }
        
        class Cell
        {
            public bool Status;
            public int Count;
            public Cell()
            {
                Status = false;
                Count = 0;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Time = !Time;
            if (Time)
            {
                timer1.Start();
                button1.Text = "Стоп";
            }
            else
            {
                timer1.Stop();
                button1.Text = "Старт";
            }

        }

        private void Reload()
        {
            for (int x = 0; x < size; x++)
                for (int y = 0; y < size; y++)
                {
                    int Sum = 0;
                    Sum += Check(x + 1, y + 1);
                    Sum += Check(x + 1, y - 1);
                    Sum += Check(x - 1, y + 1);
                    Sum += Check(x - 1, y - 1);
                    Sum += Check(x + 1, y);
                    Sum += Check(x - 1, y);
                    Sum += Check(x, y + 1);
                    Sum += Check(x, y - 1);
                    cells[x, y].Count = Sum;
                }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Reload();
            foreach (var i in cells)
            {
                if (i.Count == 3 && !i.Status)
                    i.Status = true;
                if ((i.Count > 3 || i.Count < 2) && i.Status)
                    i.Status = false;
            }
            Show();
        }

        private int Check(int x, int y)
        {
            if (x < 0 || x == size || y < 0 || y == size)
                return 0;
            return Convert.ToInt32(cells[x, y].Status);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Interval = Convert.ToInt32(1000 / Convert.ToDouble(numericUpDown1.Value.ToString()));
            Reload();
            foreach (var i in cells)
            {
                if (i.Count == 3 && !i.Status)
                    i.Status = true;
                if ((i.Count > 3 || i.Count < 2) && i.Status)
                    i.Status = false;
            }
            Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            foreach (var i in cells)
            {
                i.Count = 0;
                i.Status = false;
            }
            Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            Dictionary<string, List<Point>> MasSave = new Dictionary<string, List<Point>>();
            using (FileStream streamR = new FileStream("Save.dat", FileMode.OpenOrCreate))
            {
                try
                {
                    MasSave = formatter.Deserialize(streamR) as Dictionary<string, List<Point>>;
                    streamR.Close();
                }
                catch (Exception ex)
                {
                    //MessageBox.Show(ex.Message);
                }
            }
            List<Point> ListSave = new List<Point>();            
            for (int x = 0; x < size; x++)
                for (int y = 0; y < size; y++)
                    if (cells[x, y].Status)
                    {
                        Point p = new Point { X = x, Y = y };
                        ListSave.Add(p);                       
                    }           
            MasSave.Add(textBox1.Text, ListSave);
            FileStream stream = new FileStream("Save.dat", FileMode.Create);
            formatter.Serialize(stream, MasSave);
            stream.Close();
            listBox1.Items.Clear();
            foreach (var i in MasSave)
            {
                listBox1.Items.Add(i.Key.ToString());
            }
        }

        private void listBox1_MouseUp(object sender, MouseEventArgs e)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            Dictionary<string, List<Point>> MasSave = new Dictionary<string, List<Point>>();
            using (FileStream streamR = new FileStream("Save.dat", FileMode.OpenOrCreate))
            {
                try
                {
                    MasSave = formatter.Deserialize(streamR) as Dictionary<string, List<Point>>;
                    streamR.Close();
                }
                catch (Exception ex)
                {
                    //MessageBox.Show(ex.Message);
                }
            }
            if (e.Button == MouseButtons.Right)
            {
                using (FileStream stream = new FileStream("Save.dat", FileMode.Create))
                {
                    MasSave.Remove(listBox1.Items[listBox1.IndexFromPoint(e.X, e.Y)].ToString());
                    formatter.Serialize(stream, MasSave);
                    stream.Close();
                }
                listBox1.Items.RemoveAt(listBox1.IndexFromPoint(e.X, e.Y));
            }
            if (e.Button == MouseButtons.Left)
            {
                List<Point> ListSave = new List<Point>();
                MasSave.TryGetValue(listBox1.Items[listBox1.IndexFromPoint(e.X, e.Y)].ToString(), out ListSave);
                foreach (var i in cells)
                {
                    i.Status = false;
                    i.Count = 0;
                }
                foreach (var i in ListSave)
                {
                    cells[i.X, i.Y].Status = true;
                }
                Reload();
                Show();
            }
        }
    }
}

