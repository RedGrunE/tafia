﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Grammar2
{
    class Grammar
    {
        public List<Rule> Rules { get; set; }       

        public Grammar()
        {            
            Rules = new List<Rule>();            
        }

        public void AddRule(List<string> Input)
        {
            foreach (var i in Input)
            {
                var SplitString = i.Split(new[] { '→' }, StringSplitOptions.RemoveEmptyEntries);
                var NoTerminal = SplitString[0];
                foreach (string Action in SplitString[1].Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    Rules.Add(new Rule(NoTerminal, Action));
                }
            }
        }

        public void Generate(string InputString, int Deep, HashSet<string> Hs, int MinLength, int MaxLength, bool ReplaceEpsilon)
        {
            if (Deep >= 10) return;
            if (!(InputString.Any(x => x >= 'A' && x <= 'Z')) && InputString.Length >= MinLength && InputString.Length <= MaxLength)            
                Hs.Add(ReplaceEpsilon ? InputString : InputString.Replace("ε", ""));           
            foreach (var x in Rules.Where(x => InputString.Contains(x.NoTerminal)).ToList())
            {
                var Reg = new Regex(x.NoTerminal);
                Generate(Reg.Replace(InputString, x.Action, 1), Deep + 1, Hs, MinLength, MaxLength, ReplaceEpsilon);
                //Сюда воткнуть запись действия в строку и HashSet<string[]> Попробовать заменить Hash на List с обработкой на не повторение
                //В Листе будет два поля: 1) Сама цепочка; 2) Дерево вывода строкой
            }
        }

        public int GetTypeOfGrammar()
        {
            int TypeOfGrammar = 0;
            if (Rules.All(x => x.NoTerminal.Length <= x.Action.Length)) TypeOfGrammar = 1; else return TypeOfGrammar;
            if (Rules.All(x => x.NoTerminal.Length == 1)) TypeOfGrammar = 2; else return TypeOfGrammar;
            if (Rules.All(x => !x.Action.Where(y => char.IsUpper(y)).ToList().Any() ||
                (x.Action.Where(y => char.IsUpper(y)).ToList().Count() == 1 &&
                (x.Action.Where(y => char.IsUpper(y)).ToList().Contains(x.Action[0])) ||
                x.Action.Where(y => char.IsUpper(y)).ToList().Contains(x.Action[x.Action.Length - 1])))
            ) TypeOfGrammar = 3;
            return TypeOfGrammar;
        }

        public void DeleteOfNonProducing(string Input, Grammar MyGrammar)
        {
            var Producing = new List<char>();
            bool exit = false;
            while (!exit)
            {
                exit = true;
                foreach (var item in MyGrammar.Rules)
                {
                    bool ThisProd = true;
                    foreach (var Elements in item.Action)
                        if ((Elements >= 'A' && Elements <= 'Z') && !(Producing.Contains(Elements)))
                            ThisProd = false;
                    if (ThisProd && (!Producing.Contains(item.NoTerminal[0]) || !item.Producing))
                    {
                        item.Producing = true;
                        Producing.Add(item.NoTerminal[0]);
                        exit = false;
                    }
                }
            }
            MyGrammar.Rules.RemoveAll(x => !x.Producing);
            void ReachabilityCheck(string str, int deep)
            {
                if (deep >= 20) return;
                var l = MyGrammar.Rules.Where(x => x.NoTerminal == Convert.ToString(str[0]));
                foreach (var item in l)
                {
                    item.Reachability = true;
                    var x = item.Action.Where(char.IsUpper).ToList();
                    foreach (var i in x)
                        ReachabilityCheck(Convert.ToString(i), ++deep);
                }
            }
            ReachabilityCheck(Input, 0);
            MyGrammar.Rules.RemoveAll(x => !x.Reachability);
            //var l = MyGrammar.Rules.Select(x => new { Item = x, Cnt = x.Action.Distinct().Where(char.IsUpper).ToList() }).OrderBy(x => x.Cnt.Count).ToList();
            //var CheckedRooles = new Dictionary<string, bool>();
            //foreach (var item in l)
            //{
            //    item.Item.Producing = item.Cnt.Count == 0 || item.Cnt.All(x => CheckedRooles.Keys.Any(v => v.Contains(x)));
            //    if (!item.Item.Producing) continue;
            //    CheckedRooles[item.Item.NoTerminal] = item.Item.Producing;
            //}
            //MyGrammar.Rules.RemoveAll(x => !x.Producing);
            //MyGrammar.Rules.ForEach(item => item.Reachability = item.NoTerminal[0] == Input[0]);
        }  

        public void DeleteEps(string MainRule, Grammar MyGrammar)
        {
            bool GenerateHatch = false;
            var DistinctRules = new List<Rule>();
            var ListOfEpsilen = new List<Rule>();
            var StepBack = new List<Rule>();
            while (MyGrammar.Rules.Any(x => x.Action.Contains("ε") && x.Index != "'"))
            {
                if (MyGrammar.Rules.Any(x => x.NoTerminal == MainRule && x.Action == "ε"))
                    GenerateHatch = true;
                var RulesToAdd = new HashSet<Rule>();
                
                StepBack = ListOfEpsilen;
                ListOfEpsilen = MyGrammar.Rules.Where(x => x.Action == "ε").ToList();
                try
                {
                    if (StepBack.First().Action == ListOfEpsilen.First().Action && StepBack.First().NoTerminal == ListOfEpsilen.First().NoTerminal)
                    {
                        MyGrammar.Rules.Remove(ListOfEpsilen.First());
                        continue;
                    }
                }
                catch (Exception)
                {
                   
                }

                foreach (var item in ListOfEpsilen)
                {
                    MyGrammar.Rules.Remove(item);
                    foreach (var R in MyGrammar.Rules)
                        if (R.Action.Contains(item.NoTerminal))
                            GenerateRulesToAdd(R.NoTerminal, R.Action, item.NoTerminal[0]);
                    foreach (var NewRule in RulesToAdd)
                        MyGrammar.Rules.Add(new Rule(NewRule.NoTerminal, NewRule.Action));
                    RulesToAdd = new HashSet<Rule>();
                }
                if (GenerateHatch)
                {
                    MyGrammar.Rules.Add(new Rule(MainRule, MainRule, "'"));
                    MyGrammar.Rules.Add(new Rule(MainRule, "ε", "'"));
                }


                DistinctRules = new List<Rule>();
                foreach (var item in MyGrammar.Rules)
                    if (!DistinctRules.Any(x => x.NoTerminal == item.NoTerminal && x.Action == item.Action && x.Index == item.Index))
                        DistinctRules.Add(item);
                MyGrammar.Rules.RemoveAll(x => x.NoTerminal.Length != 0);
                foreach (var item in DistinctRules)
                {
                    MyGrammar.Rules.Add(item);
                }

                void GenerateRulesToAdd(string NT, string InputString, char SymbolOfDel)
                {
                    if (InputString.Length == 0)
                        if (!(RulesToAdd.Any(x => x.NoTerminal == NT && x.Action == "ε") || MyGrammar.Rules.Any(x => x.NoTerminal == NT && x.Action == "ε")))
                            RulesToAdd.Add(new Rule(NT, "ε"));
                    for (int i = 0; i < InputString.Length; i++)
                        if (InputString[i] == SymbolOfDel)
                        {
                            if (InputString.Remove(i, 1).Length == 0)
                            {
                                if(!(RulesToAdd.Any(x => x.NoTerminal == NT && x.Action == "ε") || MyGrammar.Rules.Any(x => x.NoTerminal == NT && x.Action == "ε")))
                                    RulesToAdd.Add(new Rule(NT, "ε"));
                            }
                            else
                            {
                                if (!(RulesToAdd.Any(x => x.NoTerminal == NT && x.Action == InputString.Remove(i, 1)) || MyGrammar.Rules.Any(x => x.NoTerminal == NT && x.Action == InputString.Remove(i, 1))))
                                    RulesToAdd.Add(new Rule(NT, InputString.Remove(i, 1)));
                            }
                            GenerateRulesToAdd(NT, InputString.Remove(i, 1), SymbolOfDel);
                        }
                }
            }
            MyGrammar.Rules = MyGrammar.Rules.OrderBy(x => x.NoTerminal).ToList();
        }

        public void CastToNFH(Grammar MyGrammar)
        {
            while (true)
            {
                bool result = AddNFH();
                if(result) break;                
            }

            bool AddNFH()
            {
                foreach (var item in MyGrammar.Rules)
                {
                    var VariableForIf = Regex.Replace(item.Action, "[(][^()]+[)]", "X");
                    if (VariableForIf.Length > 1 && (VariableForIf != VariableForIf.ToUpper() || VariableForIf.Length != 2)) //если длина больше 1 или не все большие  ?????????
                    {
                        if (item.Action[0] == item.Action.ToLower()[0]) //если первая маленькая
                        {
                            if (MyGrammar.Rules.Any(x => x.Action == item.Action[0].ToString())) // Если есть правило с таким Action
                            {
                                item.Action = MyGrammar.Rules.Where(x => x.Action == item.Action[0].ToString()).First().NoTerminal + item.Action.Remove(0, 1);
                            }
                            else // Если нет правила с таким Action
                            {
                                var index = item.Action[0];
                                item.Action = "(" + index + ")" + item.Action.Remove(0, 1);
                                MyGrammar.Rules.Add(new Rule("(" + index + ")", index.ToString(), index.ToString()));
                            }
                        }
                        VariableForIf = Regex.Replace(item.Action, "[(][^()]+[)]", "X");
                        if (MyGrammar.Rules.Any(x => x.NoTerminal == VariableForIf.Remove(0, 1))) // Если правая часть это не терминал
                            return false;
                        if (MyGrammar.Rules.Any(x => x.Action == VariableForIf.Remove(0, 1))) // если правая часть есть в правилах
                        {
                            if(item.Action[0] == '(')
                                item.Action = item.Action.Substring(0, 3) + MyGrammar.Rules.Where(x => x.Action == VariableForIf.Remove(0, 1)).First().NoTerminal;
                            else
                                item.Action = item.Action.Substring(0, 1) + MyGrammar.Rules.Where(x => x.Action == VariableForIf.Remove(0, 1)).First().NoTerminal;
                        }
                        else // если правая отсутствует в правилах
                        {
                            var index = VariableForIf.Remove(0, 1);
                            item.Action = item.Action.Substring(0, 3) + "(" + index + ")";
                            MyGrammar.Rules.Add(new Rule("(" + index + ")", index, index));
                        }
                        return false;
                    }
                }
                return true;
            }
        }

        public void DeleteLeftRecursion(Grammar MyGrammar)
        {
            DeleteOne();
            bool DeleteOne()
            {
                var RulesContainsRecursionFirst = MyGrammar.Rules.FindAll(x => x.NoTerminal == x.Action[0].ToString());
                if (RulesContainsRecursionFirst.Count != 0)
                {
                    var ListFromProcessing = MyGrammar.Rules.FindAll(x => x.NoTerminal == RulesContainsRecursionFirst.First().NoTerminal);
                }
                return true;
            }
        }

    }
}