﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace Grammar2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var MyGrammar = new Grammar();
            var Hs = new HashSet<string>();
            listBox1.Items.Clear();
            MyGrammar.AddRule(textBox1.Lines.Where(x => x != "").ToList());
            MyGrammar.Generate(textBox1.Text[0].ToString(), 0, Hs, Convert.ToInt32(numericUpDown1.Value), Convert.ToInt32(numericUpDown2.Value), checkBox1.Checked);
            Hs.ToList().ForEach(x => listBox1.Items.Add(x));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var insertText = "→";
            var selectionIndex = textBox1.SelectionStart;
            textBox1.Text = textBox1.Text.Insert(selectionIndex, insertText);
            textBox1.SelectionStart = selectionIndex + insertText.Length;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var insertText = "ε";
            var selectionIndex = textBox1.SelectionStart;
            textBox1.Text = textBox1.Text.Insert(selectionIndex, insertText);
            textBox1.SelectionStart = selectionIndex + insertText.Length;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Grammar MyGrammar = new Grammar();
            MyGrammar.AddRule(textBox1.Lines.Where(x => x != "").ToList());
            MessageBox.Show(MyGrammar.GetTypeOfGrammar().ToString() + " тип");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            var MyGrammar = new Grammar();            
            MyGrammar.AddRule(textBox1.Lines.Where(x => x != "").ToList());
            MyGrammar.DeleteOfNonProducing(textBox1.Lines[0], MyGrammar);
            textBox1.Clear();
            foreach (var item in MyGrammar.Rules)            
                textBox1.Text += item.NoTerminal + '→' + item.Action + Environment.NewLine;                      
        }

        private void button6_Click(object sender, EventArgs e)
        {
            var MyGrammar = new Grammar();
            MyGrammar.AddRule(textBox1.Lines.Where(x => x != "").ToList());
            string str = textBox1.Text[0].ToString();
            MyGrammar.DeleteEps(textBox1.Text[0].ToString(), MyGrammar);
            textBox1.Clear();
            foreach (var item in MyGrammar.Rules)
                textBox1.Text += item.NoTerminal + item.Index + '→' + item.Action + Environment.NewLine;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            var MyGrammar = new Grammar();
            MyGrammar.AddRule(textBox1.Lines.Where(x => x != "").ToList());
            MyGrammar.CastToNFH(MyGrammar);
            textBox1.Clear();
            foreach (var item in MyGrammar.Rules)
                textBox1.Text += item.NoTerminal + '→' + item.Action + Environment.NewLine;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            var MyGrammar = new Grammar();
            MyGrammar.AddRule(textBox1.Lines.Where(x => x != "").ToList());
            MyGrammar.DeleteLeftRecursion(MyGrammar);
        }
    }
}
