﻿namespace Grammar2
{
    class Rule
    {
        public string NoTerminal;
        public string Action;
        public string Index;
        public bool Producing;
        public bool Reachability;
        public Rule(string NoTerminal, string Action)
        {
            this.Action = Action;
            this.NoTerminal = NoTerminal;
            Producing = false;
            Reachability = false;
        }

        public Rule(string NoTerminal, string Action, string Index)
        {
            this.Action = Action;
            this.NoTerminal = NoTerminal;
            this.Index = Index;
            Producing = false;
            Reachability = false;
        }
    }
}
