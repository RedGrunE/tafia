namespace FLT {
    class Rule
    {
        public string neterminal;

        public string action;
        public Rule(string neterminal, string action)
        {
            this.action = action;
            this.neterminal = neterminal;
        }
    }
}