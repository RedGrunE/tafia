﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FLT
{
    class Grammar
    {
        static readonly Random rnd = new Random();
        public List<Rule> Rules { get; set; }
        public List<char> alphabet { get; set; }
         public Grammar(string alph)
        {
            Rules = new List<Rule>();
            var a = alph.Split(',').ToList();
            var chars = a.SelectMany(s => s.ToCharArray());
            alphabet = chars.Distinct().ToList();
            alphabet.Add('ξ');
            
        }

        public void AddRule(string rule)
        {
            var s = rule.Split(new[] {'→'}, StringSplitOptions.RemoveEmptyEntries);
            if (s.Length != 2) throw new Exception("Bad Input!");
            var neterminal = s[0];
            foreach (string act in s[1].Split(new [] {'|'}, StringSplitOptions.RemoveEmptyEntries))
            {
                Rules.Add(new Rule(neterminal, act));
            }
        }

        public List<char> GetNeterminals(string s)
        {
            var result = new List<char>();
            foreach(char c in s)
            {
                if (!alphabet.Contains(c)) result.Add(c);
            }
            return result;
        }

        public int getType()
        {
            var result = 0;
            if (Rules.All(x => x.neterminal.Length <= x.action.Length)) result = 1; else return result;
            if (Rules.All(x => x.neterminal.Length == 1)) result = 2; else return result;
            if (Rules.All(x => !GetNeterminals(x.action).Any() ||
                (GetNeterminals(x.action).Count()==1 &&
                (GetNeterminals(x.action).Contains(x.action[0])) ||
                GetNeterminals(x.action).Contains(x.action[x.action.Length-1]))) 
            ) result = 3;

            return result;
        }

        public void Generate(int n, System.Windows.Forms.ListBox lb, bool isFirst, string s, int deep, HashSet<string> hs)
        {
            if (deep>5 && rnd.Next(0,2)==0) return;
            if (hs.Count >= n || deep>=20) return;
            if (!HasNoTerminal(s))
            {
                hs.Add(s.Replace("ξ", ""));
                //lb.Items.Add(s.Replace("ξ","")); return; 
            }
            var rules = Rules.Where(x => s.Contains(x.neterminal)).ToList();
            foreach (var x in rules.OrderBy(x => rnd.Next()))
            {
                var regex = new Regex(x.neterminal);
                Generate(n,lb, isFirst, regex.Replace(s,x.action,1),deep+1, hs);
            }
            return;
        }

        bool HasNoTerminal(string s)
        {
            return s.Any(x => x >= 'A' && x <= 'Z');
        }

        //public string Paste(string word, HashSet<string> results)
        //{
        //    var rules = Rules.Where(x => word.Contains(x.neterminal)).ToList();
        //    if (rules.Count == 0)
        //    {
        //        return word== "ξ"?"": word;
        //    }
        //    var canNull = rules.FirstOrDefault(x => 
        //        !GetNeterminals(x.action).Any() &&
        //        !GetNeterminals(word.Replace(x.neterminal, (x.action == "ξ" ? "" : x.action))).Any() &&
        //        !results.Contains(word.Replace(x.neterminal, (x.action=="ξ"?"":x.action)))
        //    );
        //    if (canNull != null)
        //    {
        //        word = word.Replace(canNull.neterminal, (canNull.action == "ξ" ? "" : canNull.action));
        //        return word;
        //    }
        //        Random rand = new Random();
        //        Rule rule = rules[rand.Next(rules.Count)];
        //        var act = rule.action == "ξ" ? "" : rule.action;
        //        word=word.Replace(rule.neterminal, act);
        //        return Paste(word, results);
        //}




    }
}
