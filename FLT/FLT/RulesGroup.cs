﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FLT {
    class RulesGroup {
        public char Neterminal { get; set; }
        public HashSet<string> Rules { get; set; }

        public RulesGroup(string input) {
            Rules = new HashSet<string>();
            var s = input.Split('→');
            if (s.Length != 2)
                throw new Exception("Bad Input!");
            Neterminal = s[0][0];
            foreach (string act in s[1].Split('|')) {
                Rules.Add(act);
            }
        }

        public RulesGroup()
        {
            Rules = new HashSet<string>();
        }

        public void AddRules(string input) {

            var s = input.Split('→');
            if (s.Length != 2)
                throw new Exception("Bad Input!");
            if (Neterminal != s[0][0]) return;
            foreach (string act in s[1].Split('|')) {
                Rules.Add(act);
            }
        }

        public override string ToString() {
            var output = Neterminal.ToString()+ "→";
            foreach (var x in Rules) {
                output += x + "|";
            }
            return output.Remove(output.Length-1);
        }
    }
}
