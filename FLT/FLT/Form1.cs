﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FLT
{
    public partial class Form1 : Form
    {
        private string savedGrammar = "";
        public Form1()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Grammar gr = new Grammar(textBox2.Text);
            foreach(string s in textBox1.Lines)
            {
                MessageBox.Show(s);
                gr.AddRule(s);
            }
            MessageBox.Show("Тип грамматики: "+ gr.getType().ToString());
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            var insertText = "→";
            var selectionIndex = textBox1.SelectionStart;
            textBox1.Text = textBox1.Text.Insert(selectionIndex, insertText);
            textBox1.SelectionStart = selectionIndex + insertText.Length;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var insertText = "ξ";
            var selectionIndex = textBox1.SelectionStart;
            textBox1.Text = textBox1.Text.Insert(selectionIndex, insertText);
            textBox1.SelectionStart = selectionIndex + insertText.Length;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Grammar gr = new Grammar(textBox2.Text);
            listBox1.Items.Clear();
            gr = new Grammar(textBox2.Text);
            foreach (string s in textBox1.Lines.Where(x=>x!=""))
            {
                gr.AddRule(s);
            }
            var hs = new HashSet<string>();
            gr.Generate(10, listBox1, true, textBox1.Text[0].ToString(), 0, hs);
            hs.ToList().ForEach(x=>listBox1.Items.Add(x));
        }

        private void button5_Click(object sender, EventArgs e) {
            var gram = new Simplifier(textBox1.Text);
            gram.Simplifie();
            textBox1.Text = gram.ToString();
        }

        private void button6_Click(object sender, EventArgs e) {
            var gram = new Simplifier(textBox1.Text);
            gram.firstRule();
            textBox1.Text = gram.ToString();
        }

        private void button7_Click(object sender, EventArgs e) {
            var gram = new Simplifier(textBox1.Text);
            //gram.Simplifie();
            gram.secondRule();
            textBox1.Text = gram.ToString();
        }

        private void button8_Click(object sender, EventArgs e) {
            var gram = new Simplifier(textBox1.Text);
            gram.thirdRule();
            textBox1.Text = gram.ToString();
        }

        private void button9_Click(object sender, EventArgs e) {
            var gram = new Simplifier(textBox1.Text);
            gram.fourthRule();
            textBox1.Text = gram.ToString();
        }

        private void button10_Click(object sender, EventArgs e) {
            var gram = new Simplifier(textBox1.Text);
            gram.fifthRule();
            textBox1.Text = gram.ToString();
        }

        private void button11_Click(object sender, EventArgs e) {
            var gram = new Simplifier(textBox1.Text);
            gram.sixthRule();
            textBox1.Text = gram.ToString();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            savedGrammar = textBox1.Text;
        }

        private void button13_Click(object sender, EventArgs e)
        {
            textBox1.Text = savedGrammar;
        }
    }
}
