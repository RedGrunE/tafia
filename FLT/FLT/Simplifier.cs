﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace FLT {
    class Simplifier {
        private List<RulesGroup> Rules;

        public Simplifier(string allRules) {
            Rules = new List<RulesGroup>();
            foreach (var x in allRules.Replace(" ","").Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries)) {
                var rg = Rules.FirstOrDefault(y => y.Neterminal == x[0]);
                if (rg == null) {
                    Rules.Add(new RulesGroup(x));
                }
                else {
                    rg.AddRules(x);
                }
            }
        }

        /*
         E→T|E+T|E-T|ξ
T→F|F*T|F/T|ξ
F→Fn|n
             */


        public void firstRule() { //first rule: удаление, непроизводящих не терминалов.
            //foreach (var x in Rules) {
            //    if (HasTerminal(x))
            //        continue;
            //    foreach (var rule in Rules) {
            //        rule.Rules.RemoveWhere(y => y.Contains(x.Neterminal));
            //    }
            //}
            Rules.RemoveAll(x => !HasTerminalRec(x,0));
            Rules.ForEach(x => x.Rules.RemoveWhere(y => IsOnlyNeteminal(y) && Rules.All(z => z.Neterminal != y[0])));
        }

        public void secondRule() { //second rule: удаление недостижимых терминалов
            Rules.RemoveAll(
    x =>
        Rules.Except(new List<RulesGroup> { x }).All(y => y.Rules.All(z => !z.Contains(x.Neterminal))) &&
        x != Rules[0]);
        }
        public void thirdRule() { //third rule: Удаление эпсилон правил
            for (var index = 0; index < Rules.Count; index++) {
                var rule = Rules[index];
                var newRule = new RulesGroup(rule.ToString());
                foreach (var x in rule.Rules) {
                    TransformRuleByEps(x, newRule);
                }
                Rules.Remove(rule);
                rule = newRule;
                Rules.Insert(index, rule);
            }
            var addFirstEps = CanBeEps(Rules[0].Neterminal) && Rules[0].Rules.Count > 2;
            foreach (var rule in Rules) {
                rule.Rules.RemoveWhere(x => x == "ξ" && rule.Rules.Count > 2);
            }
            if (addFirstEps)
                Rules.Insert(0, new RulesGroup(GetFreeNeterminal() + "→" + Rules[0].Neterminal + "|ξ"));
        }
        public void fourthRule() {
            var kostil = 0;
            while (kostil < 5) {
                foreach (var rulesGroup in Rules.Where(x => x != Rules[0]).Where(x => x.Rules.Any(IsOnlyNeteminal))) {
                    var neterminal = rulesGroup.Rules.Where(IsOnlyNeteminal);
                    //MessageBox.Show(rulesGroup.ToString()+" " + neterminal.FirstOrDefault());
                    neterminal.ToList()
                        .ForEach(
                            n =>
                                Rules.FirstOrDefault(x => x.Neterminal == n.Replace(" ", "")[0])
                                    .Rules.ToList()
                                    .ForEach(r => rulesGroup.Rules.Add(r)));
                }
                kostil++;
            }
            Rules.Where(x => x != Rules[0]).ToList().ForEach(x => x.Rules.RemoveWhere(IsOnlyNeteminal));
        }
        public void fifthRule() {
            Rules.Select(x => x.Neterminal)
                .ToList()
                .ForEach(x => {

                    Rules.Where(y => y != Rules[0])
                        .ToList()
                        .ForEach(y => {
                            var newRuleGroup = new RulesGroup {Neterminal = GetFreeNeterminal()};
                            bool flag = false;
                            y.Rules.ToList()
                                .ForEach(z => {
                                    if (z.Length > 1 && z[0] == x) {
                                        newRuleGroup.Rules.Add(z.Remove(0, 1).Replace(" ", ""));
                                        flag = true;
                                    }
                                });
                            if (newRuleGroup.Rules.Count > 1) {
                                y.Rules.RemoveWhere(z => z[0] == x);
                                if (flag)
                                    y.Rules.Add(x.ToString() + newRuleGroup.Neterminal);
                                Rules.Add(newRuleGroup);
                            }
                        });
                });
            bool isChanged = true;
            while (isChanged) {
                isChanged = false;
                foreach (var y in Rules)
                foreach (var x in Rules) {
                    if (HashSetEqual(x.Rules, y.Rules) && x.Rules.Count > 0 && x != y) {
                        y.Rules.Clear();
                        Rules.Select(z => z.Rules)
                            .ToList()
                            .ForEach(z => z.ToList().ForEach(c => z.Add(c.Replace(y.Neterminal, x.Neterminal))));
                        Rules.Select(z => z.Rules)
                            .ToList()
                            .ForEach(z => z.RemoveWhere(c => c.Contains(y.Neterminal)));
                        isChanged = true;
                        break;
                    }
                    if (isChanged)
                        break;
                }
            }
            Rules.RemoveAll(x => x.Rules.Count == 0);
        }
        public void sixthRule() {
            var isChanged = true;
            foreach (var x in Rules.ToList()) {
                var newRuleGroup = new RulesGroup();
                var neTerminal = GetFreeNeterminal();
                newRuleGroup.Neterminal = neTerminal;
                x.Rules.Where(y => y[0] == x.Neterminal).ToList().ForEach(y => {
                    newRuleGroup.Rules.Add(y.Remove(0, 1));
                    newRuleGroup.Rules.Add(y.Remove(0, 1) + neTerminal);
                    x.Rules.RemoveWhere(z => z == y);
                    x.Rules.Add(y.Remove(0, 1) + neTerminal);
                });
                if (newRuleGroup.Rules.Count > 0)
                    Rules.Add(newRuleGroup);
            }
        }
        private static int _ruletouse = 0;
        public void Simplifie() {
            //switch (_ruletouse) {
            //    case 0:
            //        _ruletouse++;
            //        firstRule();
            //        return;
            //    case 1:
            //        _ruletouse++;
            //        secondRule();
            //        return;
            //    case 2:
            //        _ruletouse++;
            //        thirdRule();
            //        return;
            //    case 3:
            //        _ruletouse++;
            //        fourthRule();
            //        return;
            //    case 4:
            //        _ruletouse++;
            //        fifthRule();
            //        return;
            //    case 5:
            //        _ruletouse = 0;
            //        sixthRule();
            //        return;
            //}
            firstRule();
            secondRule();
            thirdRule();
            fourthRule();
            fifthRule();
            sixthRule();
        }

        private bool HashSetEqual(ICollection<string> hs1, ICollection<string> hs2)
        {
            return hs1.Count == hs2.Count && hs1.All(hs2.Contains);
        }

        private void TransformRuleByEps(string x, RulesGroup r) {
                if(x.Length == 1) return;
                for (var index = 0; index < x.Length; index++) {
                    var y = x[index];
                    if (CanBeEps(y)) {
                        var newRule = x.Remove(index, 1);
                        r.Rules.Add(newRule);
                        TransformRuleByEps(newRule,r);
                    }
                }
        }

        private char GetFreeNeterminal() {
            for (char i = 'A'; i < 'Z'; i++) {
                if (Rules.Any(x=>x.Neterminal == i)) continue;
                return i;
            }
            throw new Exception("Too many neterminals");
        }

        private bool CanBeEps(char ch) {
            return Rules.Any(x => x.Neterminal == ch && x.Rules.Any(y => y == "ξ"));
        }

        private bool HasTerminal(RulesGroup r) {
            return r != null && r.Rules.Any(IsOnlyTerminal);
        }

        private bool IsOnlyTerminal(string s) {
            return s.All(IsTerminal);
        }

        private bool IsTerminal(char ch) {
            return !(ch >= 'A' && ch <= 'Z');
        }

        private bool IsOnlyNeteminal(string s) {
            string newS = s.Replace(" ", "");
            return newS.Length == 1 && !IsTerminal(newS[0]);

        }

        private bool HasTerminalRec(RulesGroup r, int deep) {
            if (deep > 5) return false;
            return HasTerminal(r) ||
                   r.Rules.Any(
                       x => x.Any(y => !IsTerminal(y) && HasTerminalRec(Rules.FirstOrDefault(z => z.Neterminal == y), ++deep)));
        }

        public override string ToString() {
            return Rules.Aggregate("", (current, x) => current + (x + "\r\n"));
        }

    }
}
